class WordOrPhrasePhoneme < ApplicationRecord
  belongs_to :word_or_phrase
  belongs_to :phoneme
end
