class WordOrPhrase < ApplicationRecord
  has_many :word_or_phrase_phonemes, dependent: :destroy
  has_many :phonemes, through: :word_or_phrase_phonemes
  validates :text, uniqueness: true

  # Phoneme objects, in order
  def get_phonemes
    word_or_phrase_phonemes.joins(:phoneme).order('count_from_end DESC')
  end
  
  def get_phonemes_symbols
    get_phonemes.pluck(:symbol)
  end

  def get_phonemes_ids
    get_phonemes.pluck(:phoneme_id)
  end
  
 # get Phoneme_ids and pass to class level rhyme function
  def rhyme(required_match_lengh = 1)
    context= { exclude: [text] }
    phonemes_ids = get_phonemes_ids
    if phonemes.count == 0
      raise "unknown_word"
    end
    self.class.rhyme_phonemes phonemes_ids, required_match_length, context
  end

  def self.create_from(text, phonemes)
    text.gsub!(/[^A-Z ]/i, '')
    ActiveRecord::Base.transaction do
      w_or_p = WordOrPhrase.find_or_initialize_by text: text
      if w_or_p.new_record?
        phoneme_count_from_end = phonemes.length
        phonemes.each do |ph|
          phoneme = Phoneme.find_or_create_by symbol: ph
          WordOrPhrasePhoneme.find_or_create_by word_or_phrase: w_or_p, phoneme: phoneme, count_from_end: phoneme_count_from_end
          phoneme_count_from_end -= 1
        end
        w_or_p.save!
      end
    end
  end
  
  def self.rhyme(text, required_match_length = 2)
    text.upcase!
    phonemes_ids = derive_phoneme_ids_from(text)
    context = { exclude: [text]}
    rhyme_phonemes phonemes_ids, required_match_length, context
  end

  def self.sequence_rhyme(text)
    text.upcase!
    phonemes_ids = derive_phoneme_ids_from(text)
    context = {
      exclude: text.split(/\W+/).reject {|w| w.length < 3 }
    }
    WordOrPhrase.transaction do
      recursive_rhyme_sequence_finder phonemes_ids, context
    end
  end

  private
  # exclusive rhyme query
  def self.required_phonemes(phonemes_ids, count, query = WordOrPhrase.all)
    phonemes_ids.reverse.each_with_index do |ph_id, i|
      ids = WordOrPhrasePhoneme.where(phoneme_id: ph_id, count_from_end: i+1).select('word_or_phrase_id')
      query = query.where id: ids
      if i == count - 1 then break end
    end
    query
  end
  
  # order rhyme results query
  def self.optional_phonemes(phonemes_ids, skip, query)
    condition = option_phonemes_condition phonemes_ids, skip
    query.left_outer_joins(:word_or_phrase_phonemes).where(condition).group(:id).order('COUNT(word_or_phrase_phonemes.id) DESC')
  end

  def self.option_phonemes_condition(phonemes_ids, skip)
    phonemes_ids = phonemes_ids.reverse.each_with_index.reject {|ph, i| i < skip}
    (phonemes_ids.map {|ph_id, i|  "(word_or_phrase_phonemes.phoneme_id=#{ph_id} AND word_or_phrase_phonemes.count_from_end=#{i+1})"}).join ' OR '
  end

  def self.rhyme_phonemes(phonemes_ids, required_match_length, context)
    query = WordOrPhrase.where.not(text:context[:exclude])
    query = required_phonemes phonemes_ids, required_match_length, query
    optional_phonemes phonemes_ids, required_match_length, query
  end

  def self.recursive_rhyme_sequence_finder(phonemes_ids, context)
    #start at end
    required_phonemes = [10, phonemes_ids.length].max
    while true
      result = rhyme_phonemes phonemes_ids, required_phonemes, context
      #debugger
      if result.length != 0
        # have a result!
        result_phonemes_ids = result[0].get_phonemes_ids
        slice_index_start = [ 0, phonemes_ids.length - result_phonemes_ids.length ].max
        new_phonemes_ids = phonemes_ids
        new_phonemes_ids.slice!(slice_index_start, result_phonemes_ids.length) # remove the count of phonemes that the match takes
        debugger
        return recursive_rhyme_sequence_finder_continue(new_phonemes_ids, result, context)
      else 
        required_phonemes -= 1
        if required_phonemes == 0 # nothing found here
          new_phonemes_ids = phonemes_ids.slice (phonemes_ids.length - 1),1 # remove one phoneme from the end
          return recursive_rhyme_sequence_finder_continue(new_phonemes_ids, nil, context)
        end
      end
    end
  end
  
  # reusable code for different end points in main function
  def self.recursive_rhyme_sequence_finder_continue(new_phoneme_ids, result, context)
    result_value = if result then result.limit(10).pluck(:text) else nil end
    if new_phoneme_ids.empty? # done
      return [result_value]
    else
      return recursive_rhyme_sequence_finder(new_phoneme_ids, context).concat [result_value]
    end
  end
  
  #for phrases
  def self.derive_phoneme_ids_from(text)
    words = text.split ' '
    (words.map {|w| get_phonemes_for_word w }).flatten
  end

  # for words ;)
  def self.get_phonemes_for_word(word)
    existing_word = find_by(text: word)
    if existing_word 
      existing_word.get_phonemes_ids
    else
      print "i do not understand #{word}"
      throw :unknown_word
    end
  end
  # realness
  def self.where_clause(phonemes)
    (phonemes.reverse.each_with_index.map { |phoneme, i| "(word_or_phrase_phonemes.phoneme_id = #{phoneme.id} AND word_or_phrase_phonemes.count_from_end = #{i})"}).join ' OR '
  end

end
