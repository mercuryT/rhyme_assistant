# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

def do_batch(lines)
  ActiveRecord::Base.transaction do 
    lines.each do |line|
      begin
        unless line.match ';;;'
          word, phonemes = line.split ' ', 2
          phonemes = phonemes.split ' '
          WordOrPhrase.create_from word, phonemes
        end
      rescue ArgumentError
        print "cannot parse #{line}"
      end
    end
  end
end


File.open("CMU_dict.7b") do |file|
  file.lazy.each_slice(100) do |lines|
    do_batch lines
  end
end

