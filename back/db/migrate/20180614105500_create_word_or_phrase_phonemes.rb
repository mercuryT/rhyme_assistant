class CreateWordOrPhrasePhonemes < ActiveRecord::Migration[5.2]
  def change
    create_table :word_or_phrase_phonemes do |t|
      t.references :word_or_phrase, foreign_key: true
      t.references :phoneme, foreign_key: true
      t.integer :count_from_end
    end
  end
end
